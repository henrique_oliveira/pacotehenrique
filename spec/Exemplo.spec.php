<?php 

use dz004\novopacote\Exemplo;

describe('Exemplo', function(){

    describe('nome', function(){
        it('Contém pacote', function(){
            $e = new Exemplo();
            expect( $e->nome() )->toContain( 'pacote') ;
        });
    });
});